#include <pwd.h>
#include <wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

int main(void)
{
	pid_t child_id;
	int status;
	DIR *dp;
	struct dirent *ep;

	child_id = fork();

	if (child_id < 0) exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti

	if (child_id == 0)
	{ // Make darat dir
		char *argv[] = {"mkdir", "-p", "/home/richo/modul2/darat", NULL};
		execv("/bin/mkdir", argv);
	}
	else
	{ 
		while ((wait(&status)) > 0);
		child_id = fork();

		if (child_id == 0)
		{ // After 3 second make air dir
			sleep(3);
			char *argv[] = {"mkdir", "-p", "/home/richo/modul2/air", NULL};
			execv("/bin/mkdir", argv);
		}
		else
		{ 
			while ((wait(&status)) > 0);
			child_id = fork();

			if (child_id == 0)
			{ // Unzip
				while ((wait(&status)) > 0);
				char *argv[] = {"unzip", "/home/richo/modul2/animal.zip", "-d", "/home/richo/modul2", NULL};
				execv("/bin/unzip", argv);
			}
			else
			{ 
				while ((wait(&status)) > 0);
				child_id = fork();

				if (child_id == 0)
				{ // Move each file
					while ((wait(&status)) > 0);
					char path[100] = "/home/richo/modul2/animal/";
					dp = opendir(path);
					if (dp != NULL)
					{
						while (ep = readdir(dp))
						{
							if (strstr(ep->d_name, "darat"))
							{
								char oriFilePath[100] = "/home/richo/modul2/animal/";
								char newFilePath[100] = "/home/richo/modul2/darat/";
								strcat(oriFilePath, ep->d_name);
								strcat(newFilePath, ep->d_name);
								
								FILE *oriFile, *newFile;
								oriFile = fopen(oriFilePath, "rb");
								newFile = fopen(newFilePath, "ab");

								while(1) {
									int c = fgetc(oriFile);
									if (feof(oriFile)) break;
									fputc(c, newFile);
								}

								fclose(oriFile);
								fclose(newFile);
								remove(oriFilePath);
							}
							else if (strstr(ep->d_name, "air"))
							{
								char oriFilePath[100] = "/home/richo/modul2/animal/";
								char newFilePath[100] = "/home/richo/modul2/air/";
								strcat(oriFilePath, ep->d_name);
								strcat(newFilePath, ep->d_name);
								
								FILE *oriFile, *newFile;
								oriFile = fopen(oriFilePath, "rb");
								newFile = fopen(newFilePath, "ab");

								while(1) {
									int c = fgetc(oriFile);
									if (feof(oriFile)) break;
									fputc(c, newFile);
								}

								fclose(oriFile);
								fclose(newFile);
								remove(oriFilePath);
							}
							else
							{
								char filePath[100] = "/home/richo/modul2/animal/";
								strcat(filePath, ep->d_name);
								remove(filePath);
							}
						}
					}
					char *argv[] = {"rm", "-d", "/home/richo/modul2/animal", NULL};
					execv("/bin/rm", argv);
				}
				else
				{ 
					while ((wait(&status)) > 0);
					child_id = fork();

					if (child_id == 0)
					{ // Deleting bird
						while ((wait(&status)) > 0);
						dp = opendir("/home/richo/modul2/darat/");
						if (dp != NULL)
						{
							while (ep = readdir(dp))
							{
								if (strstr(ep->d_name, "bird"))
								{
									char filePath[100] = "/home/richo/modul2/darat/";
									strcat(filePath, ep->d_name);
									remove(filePath);
								}
							}
						}
					}
					else
					{ // Making list
						while ((wait(&status)) > 0);
						FILE *listFile;
						listFile = fopen("/home/richo/modul2/air/list.txt", "w+");

						dp = opendir("/home/richo/modul2/air/");
						if (dp != NULL)
						{

							while (ep = readdir(dp))
							{
								if (strstr(ep->d_name, "air")) {
									char fileNameFormatted[100] = "";
									struct stat info;
									int r;
									
									char oriFileName[100] = "";
									strcat(oriFileName, ep->d_name);

									char filePath[100] = "/home/richo/modul2/air/";
									strcat(filePath, oriFileName);
									
									r = stat(filePath, &info);
									if( r==-1 )
									{
										fprintf(stderr,"File error\n");
										exit(1);
									}

									struct passwd *pw = getpwuid(info.st_uid);

									char user[20] = "";
									if (pw != 0) strcat(user, pw->pw_name);
									strcat(user, "_");

									char permission[5] = "";
									if( info.st_mode & S_IRUSR )
										strcat(permission, "r");
									if( info.st_mode & S_IWUSR )
										strcat(permission, "w");
									if( info.st_mode & S_IXUSR )
										strcat(permission, "x");
									strcat(permission, "_");
									
									strcat(fileNameFormatted, user);
									strcat(fileNameFormatted, permission);
									strcat(fileNameFormatted, oriFileName);
									fprintf(listFile, "%s\n", fileNameFormatted);
								}
							}
						}

						fclose(listFile);
					}
				}
			}
		}
	}
}